import React, {Component} from 'react'
import {View} from 'react-native'
import I18n from 'react-native-i18n'
import {Provider} from 'react-redux'
import {persistStore} from 'redux-persist';
import {AsyncStorage} from 'react-native';

import locales from './locales';
import RouterView from './containers/router'

import createStore from './store'

const store = createStore();

global.self = null;

I18n.fallbacks = true;
I18n.translations = locales;

export default class TooMuchPassword extends Component {

  constructor(props) {
    super(props);
    this.state = {rehydrated: false};
  }

  componentDidMount() {
    persistStore(store, {
      storage: AsyncStorage,
      whitelist: [
        'account',
        'authentication',
        'configuration',
        'password',
        // 'router',
      ],
    }, () => {
      this.setState({rehydrated: true});
    })
  }

  render() {
    if (!this.state.rehydrated) {
      return (<View />);
    }
    return (
      <Provider store={store}>
        <RouterView />
      </Provider>
    );
  }

}
