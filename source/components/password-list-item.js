import React, {Component, PropTypes} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import I18n from 'react-native-i18n';
import {Card} from 'react-native-material-design';

import {typography} from 'react-native-material-design-styles';
import moment from 'moment';

export default class PasswordListItem extends Component {

  static propTypes = {
    password: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      createdAt: PropTypes.string.isRequired,
    }),
  };

  render() {
    return (
      <Card>
        <Card.Body>
          <Text style={styles.title}>{this.props.password.title}</Text>
          <Text style={styles.subtitle}>{moment(this.props.password.createdAt).format('LLL')}</Text>
        </Card.Body>
      </Card>
    );
  }

}

const styles = StyleSheet.create({
  title: {
    ...typography.paperFontSubhead,
    lineHeight: 24,
  },
  subtitle: {
    ...typography.paperFontBody1,
    lineHeight: 22,
    fontSize: 14,
  }
});
