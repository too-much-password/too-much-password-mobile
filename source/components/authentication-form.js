import React, {Component, PropTypes} from 'react';
import {Image, View, StyleSheet, Text, TextInput} from 'react-native';
import I18n from 'react-native-i18n';
import {Button} from 'react-native-material-design';
import isEmail from 'validator/lib/isEmail';
import isLength from 'validator/lib/isLength';

export default class AuthenticationForm extends Component {

  static propTypes = {
    credentials: PropTypes.shape({
      email: PropTypes.string.isRequired,
      password: PropTypes.string.isRequired,
    }).isRequired,
    onLogin: PropTypes.func.isRequired,
    onRegister: PropTypes.func.isRequired,
    loading: PropTypes.bool.isRequired,
  };

  onChange(field, value) {
    this.props.onChange({
      ...this.props.credentials,
      [field]: value
    });
  }

  isValid() {
    return !this.props.loading &&
      isEmail(this.props.credentials.email) &&
      isLength(this.props.credentials.password, {min: 6});
  }

  render() {
    let valid = this.isValid();
    return (
      <View style={styles.view}>
        <View style={styles.imageRow}>
          <Image
            source={require('../../logo/logo_128.png')}
            style={styles.image}
          />
        </View>
        <View style={styles.row}>
          <TextInput
            blurOnSubmit={false}
            editable={!this.props.loading}
            keyboardType="email-address"
            onChange={e => this.onChange('email', e.nativeEvent.text)}
            placeholder={I18n.t('account.email')}
            returnKeyType="next"
            value={this.props.credentials.email}
          />
        </View>
        <View style={styles.row}>
          <TextInput
            blurOnSubmit={false}
            editable={!this.props.loading}
            keyboardType="default"
            onChange={e => this.onChange('password', e.nativeEvent.text)}
            placeholder={I18n.t('account.password')}
            returnKeyType="next"
            secureTextEntry={true}
            value={this.props.credentials.password}
          />
        </View>
        <View style={styles.row}>
          <Button
            disabled={!valid || this.props.loading}
            onPress={this.props.onLogin}
            raised={true}
            text={I18n.t('account.log-in')}
            value="Log in"
          />
        </View>
        <View style={styles.row}>
          <Button
            disabled={!valid || this.props.loading}
            onPress={this.props.onRegister}
            text={I18n.t('account.sign-in')}
            value="Register"
          />
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    padding: 40,
  },
  imageRow: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
  },
  row: {
  }
});
