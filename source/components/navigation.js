import React, {Component, PropTypes} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {Drawer} from 'react-native-material-design';
import I18n from 'react-native-i18n';

export default class Navigation extends Component {

  static propTypes = {
    navigateTo: PropTypes.func.isRequired,
    onLogout: PropTypes.func.isRequired,
    currentState: PropTypes.string.isRequired,
  };

  render() {
    let sectionHome = [
      {
        icon: 'home',
        value: I18n.t('navigation.home'),
        active: this.props.currentState && this.props.currentState === 'home',
        onPress: () => this.props.navigateTo('home'),
        onLongPress: () => this.props.navigateTo('home')
      },
      {
        icon: 'person',
        value: I18n.t('navigation.account'),
        active: this.props.currentState && this.props.currentState === 'account',
        onPress: () => this.props.navigateTo('account'),
        onLongPress: () => this.props.navigateTo('account')
      },
      {
        icon: 'build',
        value: I18n.t('navigation.configuration'),
        active: this.props.currentState && this.props.currentState === 'configuration',
        onPress: () => this.props.navigateTo('configuration'),
        onLongPress: () => this.props.navigateTo('configuration')
      },
      {
        icon: 'close',
        value: I18n.t('navigation.logout'),
        active: false,
        onPress: () => this.props.onLogout(),
        onLongPress: () => this.props.onLogout()
      }
    ];
    return (
      <Drawer theme="light">
        <Drawer.Header>
          <Image
            source={require('../resources/navigation-header.png')}
            style={styles.image}
          />
        </Drawer.Header>
        <Drawer.Section items={sectionHome} />
      </Drawer>
    );
  }

};

const styles = StyleSheet.create({
  image: {
    alignSelf: 'center',
    flex: 1,
    width: 150,
  }
});
