import React, {Component, PropTypes} from 'react';
import {Clipboard, StyleSheet, Text, View} from 'react-native';
import {Avatar, Button} from 'react-native-material-design';
import {typography, color} from 'react-native-material-design-styles';

export default class PastableValue extends Component {

  static PropTypes = {
    onChange: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    value: PropTypes.string,
  };

  onPaste() {
    Clipboard
      .getString()
      .then(res => this.props.onChange(res));
  }

  renderAvatar() {
    if (this.props.value) {
      return (
        <Avatar
          backgroundColor={color.paperGreen500.color}
          icon="check"
        />
      );
    } else {
      return (
        <Avatar
          backgroundColor={color.paperRed500.color}
          icon="close"
        />
      );
    }
  }

  render() {
    return (
      <View style={styles.view}>
        {this.renderAvatar()}
        <Text style={styles.text}>{this.props.text}</Text>
        <Button
          onPress={e => this.onPaste()}
          text="Paste"
        />
      </View>
    );
  }

};

const styles = StyleSheet.create({
  view: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    padding: 10,
  },
  text: {
    ...typography.paperFontBody1,
    flex: 1
  }
});
