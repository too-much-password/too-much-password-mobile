import React, {Component, PropTypes} from 'react';
import {Image, View, StyleSheet, Text, TextInput} from 'react-native';
import I18n from 'react-native-i18n';
import {Button} from 'react-native-material-design';
import isEmail from 'validator/lib/isEmail';
import isLength from 'validator/lib/isLength';

export default class ConfigurationForm extends Component {

  static propTypes = {
    configuration: PropTypes.shape({
      email: PropTypes.string.isRequired,
      password: PropTypes.string.isRequired,
    }).isRequired,
    onChange: PropTypes.func.isRequired,
  };

  render() {
    return (
      <View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
});
