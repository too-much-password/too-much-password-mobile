import React, {Component, PropTypes} from 'react';
import {ActivityIndicator, Clipboard, View, StyleSheet, Text} from 'react-native';
import I18n from 'react-native-i18n';
import {Button} from 'react-native-material-design';

import {asymetricDecrypt} from '../actions/crypto';

import {typography} from 'react-native-material-design-styles';
import moment from 'moment';
import {find} from 'lodash';

export default class PasswordCopier extends Component {

  static propTypes = {
    account: PropTypes.shape({
      id: PropTypes.number.isRequired,
      publicKey: PropTypes.string.isRequired,
    }),
    privateKey: PropTypes.string,
    password: PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      createdAt: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
    }),
  };

  state = {processing: false};

  onCopy = () => {
    this.setState({processing: true});
    let encCipherKey = find(this.props.password.cipherKeys, {accountId: this.props.account.id});
    /*
    crypto.asymetric
      .decrypt(this.props.privateKey, encCipherKey.content)
      .then(cipherKey => crypto.symetric.decrypt(cipherKey, this.props.password.content))
      .then(password => Clipboard.setString(password))
      .then(() => this.setState({processing: false}));
      .catch(err => {
        this.setState({processing: false});
        throw err;
      });
    */
  }

  render() {
    if (this.state.processing) {
      return (<ActivityIndicator animating={true} size="large" />);
    } else {
      return (
        <Button
          onPress={this.onCopy}
          raised={true}
          text="Copy"
        />
      );
    }
  }

}

const styles = StyleSheet.create({
});
