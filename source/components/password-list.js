import React, {Component, PropTypes} from 'react';
import {View, StyleSheet, TouchableHighlight} from 'react-native';

import PasswordListItem from './password-list-item';

export default class PasswordList extends Component {

  static propTypes = {
    passwords: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      createdAt: PropTypes.string.isRequired,
    })),
    onSelect: PropTypes.func.isRequired,
  };

  render() {
    return (
      <View>
        {this.props.passwords.map(item => (
          <TouchableHighlight key={item.id} onPress={e => this.props.onSelect(item)}>
            <View>
              <PasswordListItem password={item} />
            </View>
          </TouchableHighlight>
        ))}
      </View>
    );
  }

}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    padding: 40,
  },
  row: {
  }
});
