import * as cmd from '../reducers/password';
import {action as request} from 'too-much-password-networking';

export const fetch = () => {
  return (dispatch, getState) => {
    const {token} = getState().authentication;
    return dispatch(request(`https://too-much-password.herokuapp.com/api/accounts/${token.userId}/passwords`))
      .then(res => {
        if (res.status === 200) {
          dispatch(cmd.set(res.data));
        }
        return res;
      })
  }
};
