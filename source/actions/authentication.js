import * as cmd from '../reducers/authentication';
import {action as request} from 'too-much-password-networking';

export const login = (credentials) => {
  return dispatch => {
    return dispatch(request('https://too-much-password.herokuapp.com/api/accounts/login', {
      method: 'POST',
      body: JSON.stringify(credentials),
    })).then(res => {
      if (res.status === 200) {
        console.log(res.data);
        dispatch(cmd.login(res.data));
      }
      return res;
    }).catch(err => {
      console.log(err);
      throw err;
    });
  }
};

export const logout = () => {
  return dispatch => dispatch(cmd.logout())
};
