import {decrypt, key, message} from 'react-native-openpgp';

export const asymetric = {
  decrypt: (privateKey, content) => {
    let options = {
      message: message.readArmored(content),
      privateKey: key.readArmored(privateKey).keys[0],
    };
    return decrypt(options).then(res => res.data);
  }
};

export const symetric = {
  decrypt: (privateKey, content) => {
  }
};

export default {asymetric, symetric};
