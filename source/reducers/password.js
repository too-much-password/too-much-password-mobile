import * as auth from './authentication';
/**
 * Constants
 */

export const SET = 'PASSWORD_SET';
export const ADD = 'PASSWORD_ADD';

/**
 * Action
 */

export const set = list => ({type: SET, list});
export const stop = password => ({type: ADD, password});

/**
 * Reducer
 */

export default (state = [], action) => {
  switch(action.type) {
    case SET:
      return [...action.list];
    case ADD:
      return [action.password, ...state];
    case auth.LOGOUT:
      return [];
    default:
      return state;
  }
};
