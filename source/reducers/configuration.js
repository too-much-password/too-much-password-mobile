/**
 * Constants
 */

export const SET = 'CONFIGURATION_SET';

/**
 * Action
 */

export const set = (key, value) => ({type: SET, payload: {key, value}});

/**
 * Reducer
 */

export default (state = {}, action) => {
  switch(action.type) {
    case SET:
      return {
        ...state,
        [action.payload.key]: action.payload.value
      };
    default:
      return state;
  }
};
