export const PUSH = 'ROUTER_PUSH';

export const push = (name, params = null) => ({type: PUSH, payload: {name, params}});

/**
 * Reducer
 */

export const initialState = {name: 'home', params: null};

export default (state = initialState, action) => {
  switch(action.type) {
    case PUSH:
      return action.payload;
    default:
      return state;
  }
}
