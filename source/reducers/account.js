/**
 * Constants
 */

export const SET = 'ACCOUNT_SET';
export const SET_PRIVATE_KEY = 'ACCOUNT_SET_PRIVATE_KEY';

/**
 * Action
 */

export const set = (account) => {
  return {type: SET, account}
};
export const setPrivateKey = (privateKey) => {
  return {type: SET_PRIVATE_KEY, privateKey}
};

/**
 * Reducer
 */

const initialState = {
  account: null,
  privateKey: null
};

export default (state = initialState, action) => {
  switch(action.type) {
    case SET:
      return {
        ...state,
        account: action.account
      };
    case SET_PRIVATE_KEY:
      return {
        ...state,
        privateKey: action.privateKey
      };
    default:
      return state;
  }
};
