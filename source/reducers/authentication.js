/**
 * Constants
 */

export const LOGIN = 'AUTHENTICATION_LOGIN';
export const LOGOUT = 'AUTHENTICATION_LOGOUT';

/**
 * Action
 */

export const login = (token) => {
  return {type: LOGIN, token}
};
export const logout = () => {
  return {type: LOGOUT}
};

/**
 * Reducer
 */

const initialState = {
  token: null
};

export default (state = initialState, action) => {
  switch(action.type) {
    case LOGIN:
      return {token: action.token};
    case LOGOUT:
      return {token: null};
    default:
      return state;
  }
};
