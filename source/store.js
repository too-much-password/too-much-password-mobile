import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import {autoRehydrate} from 'redux-persist';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

import * as rootReducer from './reducers';

export default function(data = {}) {
  const logger = createLogger();
  const middleware = applyMiddleware(thunk, logger);
  const reducers = combineReducers(rootReducer);
  return compose(autoRehydrate())(createStore)(reducers, data, middleware);
}
