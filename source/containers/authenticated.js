import React, {Component} from 'react';
import {DrawerLayoutAndroid, StyleSheet, View} from 'react-native';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {Toolbar} from 'react-native-material-design';

import * as AuthenticationActions from '../actions/authentication';
import Navigation from '../components/navigation';
import {push} from '../reducers/router';

class AuthenticatedView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      drawer: null,
      navigation: null,
    };
  }

  componentWillMount() {
    if (!this.props.authentication.token) {
      this.props.routerActions.push('authentication');
    }
  }

  openDrawer() {
    if (!this.state.navigation) {
      this.setState({
        navigation: (
          <Navigation
            navigateTo={this.props.routerActions.push}
            onLogout={() => this.onLogout()}
            currentState="home"
          />
        )
      });
    }
    this.state.drawer.openDrawer();
  }

  onLogout() {
    this.props.authenticationActions.logout();
    this.props.routerActions.push('authentication');
  }

  render() {
    return (
      <DrawerLayoutAndroid
        drawerWidth={300}
        drawerPosition={DrawerLayoutAndroid.positions.Left}
        ref={drawer => {!this.state.drawer ? this.setState({drawer}) : null}}
        renderNavigationView={() => this.state.navigation}
      >
        <View style={styles.container}>
          <Toolbar
            icon="menu"
            onIconPress={() => this.openDrawer()}
            title="Too Much Password !"
          />
          {this.props.children}
        </View>
      </DrawerLayoutAndroid>
    );
  }

}

const styles = StyleSheet.create({
  container: {paddingTop: 56},
  view: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default connect(
  function(state) {
    return {
      authentication: state.authentication
    }
  },
  function(dispatch) {
    return {
      authenticationActions: bindActionCreators(AuthenticationActions, dispatch),
      routerActions: bindActionCreators({push}, dispatch),
    }
  }
)(AuthenticatedView); 
