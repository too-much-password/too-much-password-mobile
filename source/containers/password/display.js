import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {find} from 'lodash';

import {push} from '../../reducers/router';

import PasswordCopier from '../../components/password-copier';
import PasswordListItem from '../../components/password-list-item';

import I18n from 'react-native-i18n';

class PasswordDisplayView extends Component {

  render() {
    return (
      <View style={styles.view}>
        <PasswordListItem
          password={this.props.password}
        />
        <PasswordCopier
          password={this.props.password}
          account={this.props.account.account}
          privateKey={this.props.account.privateKey}
        />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  view: {
  },
});

export default connect(
  function(state) {
    return {
      account: state.account,
      password: find(state.password, state.router.params),
    }
  },
  function(dispatch) {
    return {
      routerActions: bindActionCreators({push}, dispatch),
    }
  }
)(PasswordDisplayView); 
