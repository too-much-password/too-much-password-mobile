import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {push} from '../reducers/router';

import PastableValue from '../components/pastable-value';

import * as AuthenticationActions from '../actions/authentication';
import * as AccountActions from '../actions/account';

import I18n from 'react-native-i18n';

class AccountView extends Component {

  state = {loading: false};

  onChangePublicKey = value => {
  }

  onChangePrivateKey = value => {
    this.props.accountActions.setPrivateKey(value);
  }

  render() {
    return (
      <View style={styles.view}>
        <PastableValue
          onChange={this.onChangePublicKey}
          text={I18n.t('account.public-key')}
          value={this.props.account.account.publicKey}
        />
        <PastableValue
          onChange={this.onChangePrivateKey}
          text={I18n.t('account.private-key')}
          value={this.props.account.privateKey}
        />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default connect(
  function(state) {
    return {
      authentication: state.authentication,
      account: state.account,
    }
  },
  function(dispatch) {
    return {
      accountActions: bindActionCreators(AccountActions, dispatch),
      authenticationActions: bindActionCreators(AuthenticationActions, dispatch),
      routerActions: bindActionCreators({push}, dispatch),
    }
  }
)(AccountView); 
