import React, {Component} from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {push} from '../reducers/router';

import AuthenticationForm from '../components/authentication-form';

import * as AuthenticationActions from '../actions/authentication';

class AuthenticationView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false
    };
  }

  onChange(credentials) {
    this.setState({credentials});
  }

  onLogin() {
    this.setState({loading: true});
    this.props.authenticationActions
      .login(this.state.credentials)
      .then(() => this.setState({loading: false}))
      .catch(err => {
        this.setState({loading: false});
        console.error(JSON.stringify(err));
        throw err;
      })
      .then(() => this.props.routerActions.push('home'))
  }

  onRegister() {
    console.log(this.state.credentials);
  }

  render() {
    return (
      <View style={styles.view}>
        <AuthenticationForm
          credentials={this.state.credentials}
          loading={this.state.loading}
          onChange={credentials => this.onChange(credentials)}
          onLogin={() => this.onLogin()}
          onRegister={() => this.onRegister()}
        />
      </View>
    );
  }

}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default connect(
  function(state) {
    return {
      authentication: state.authentication
    }
  },
  function(dispatch) {
    return {
      authenticationActions: bindActionCreators(AuthenticationActions, dispatch),
      routerActions: bindActionCreators({push}, dispatch),
    }
  }
)(AuthenticationView); 
