import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux'

import AuthenticationView from './authentication'
import AuthenticatedView from './authenticated'
import AccountView from './account'
import HomeView from './home'
import PasswordDisplayView from './password/display';

class RouterView extends Component {

  renderAnonymous() {
    switch(this.props.router.name) {
      case 'authentication':
        return <AuthenticationView />;
      default:
        return <View />;
    }
  }

  renderAuthenticated() {
    switch(this.props.router.name) {
      case 'account':
        return <AccountView />;
      case 'password.display':
        return <PasswordDisplayView />;
      case 'configuration':
      case 'home':
        return <HomeView />;
      default:
        return <View />;
    }
  }

  render() {
    switch(this.props.router.name) {
      default:
      case 'authentication':
        return this.renderAnonymous();
      case 'account':
      case 'configuration':
      case 'home':
      case 'password.display':
        return (
          <AuthenticatedView>
            {this.renderAuthenticated()}
          </AuthenticatedView>
        );
    }
  }

}

export default connect(
  function(state) {
    return {
      router: state.router
    }
  },
  function() {
    return {}
  }
)(RouterView); 
