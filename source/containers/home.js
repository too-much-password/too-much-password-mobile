import React, {Component} from 'react';
import {RefreshControl, ScrollView, StyleSheet, View} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {Toolbar} from 'react-native-material-design';
import {Card} from 'react-native-material-design';

import PasswordList from '../components/password-list';

import {push} from '../reducers/router';
import * as AccountActions from '../actions/account';
import * as PasswordActions from '../actions/password';

class HomeView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isRefreshing: false
    };
  }

  componentDidMount() {
    if (this.props.authentication.token && this.props.passwords.length == 0) {
      this.props.accountActions.fetch()
        .then(() => this.props.passwordActions.fetch())
    }
  }

  onRefresh() {
    this.setState({isRefreshing: true});
    this.props.passwordActions.fetch()
      .then(() => this.setState({isRefreshing: false}));
  }

  onOpen(password) {
    this.props.routerActions.push('password.display', {id: password.id});
  }

  render() {
    return (
      <View>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={() => this.onRefresh()}
            />
          }
        >
          <PasswordList
            passwords={this.props.passwords}
            onSelect={password => this.onOpen(password)}
          />
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {paddingTop: 56},
  view: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  row: {}
});

export default connect(
  function(state) {
    return {
      account: state.account,
      authentication: state.authentication,
      passwords: state.password,
    }
  },
  function(dispatch) {
    return {
      accountActions: bindActionCreators(AccountActions, dispatch),
      passwordActions: bindActionCreators(PasswordActions, dispatch),
      routerActions: bindActionCreators({push}, dispatch),
    }
  }
)(HomeView); 
