import 'react-native';
import React from 'react';
import PasswordCopier from '../source/components/password-copier';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

it('renders correctly', () => {
  const account = {id: 1, publicKey: ''};
  const privateKey = '';
  const password = {id: 1, title: 'title', createdAt: new Date().toString(), content: ''};
  const tree = renderer.create(
    <PasswordCopier
      account={account}
      privateKey={privateKey}
      password={password}
    />
  ).toJSON();
});
